// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import {
    getDatabase,
    onValue,
    ref,
    set,
    child,
    get,
    update,
    remove
  }
  from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCxqjL3hpwaZaJpYh7z4v8ziu-YEAKDwSk",
  authDomain: "pagina-web-1ace2.firebaseapp.com",
  databaseURL: "https://pagina-web-1ace2-default-rtdb.firebaseio.com",
  projectId: "pagina-web-1ace2",
  storageBucket: "pagina-web-1ace2.appspot.com",
  messagingSenderId: "215896948377",
  appId: "1:215896948377:web:fc1fceea15e7d057270208"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

window.onload = function mostrarDesarroladores() {
    const db = getDatabase();
    const dbRef = ref(db, 'Miembros/Desarroladores');
  
    onValue(dbRef, (snapshot) => {
        mostrarMiebros.innerHTML = ""
        snapshot.forEach((childSnapshot) => {
  
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();

            mostrarMiebros.innerHTML = "<div class='card-miembros'>" +
            "<div class='service-image'>" +
              "<img class='imgMiembros' src='" + childData.Imagen + "' alt=''>" +
            "</div>" +
            "<div class='info-cards'>" +
              "<h3 class='nombreMiembros'>" + childData.Nombre + "</h3>" +
            "</div>" +
              "<button type='button' class='btnLeerMas'>Leer Más</button>" +
          "</div>" + mostrarMiebros.innerHTML;



            console.log(childKey + ":");
            console.log("" + childData.Nombre)
        });
    }, {
        onlyOnce: true
    });
  }